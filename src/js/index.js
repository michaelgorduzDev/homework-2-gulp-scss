// Check screen width on page load and whenever the window is resized
function checkScreenWidth() {
  let screenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  if (screenWidth <= 320) {
    document.getElementById("footer-p").innerHTML = "Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio.";
  } else {
    document.getElementById("footer-p").innerHTML = "Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio. Curabitur blandit tempus porttitor vollisky inceptos mollisestor.";
  }
}

window.onload = function () {
  checkScreenWidth();
};

window.onresize = function () {
  checkScreenWidth();
};

// Hamburger menu
const hamburger = document.querySelector(".hamburger");
const menu = document.querySelector(".nav-links");

hamburger.addEventListener("click", () => {
  hamburger.classList.toggle("active");
  menu.classList.toggle("active");
})
